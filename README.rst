==========
Mailserver
==========

A Vagrantfile and setup script for creating a Debian based mailserver running
Exim4, Dovecot, and Spam Assassin.
