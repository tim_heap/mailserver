#!/bin/bash

set -e

config="/vagrant/config.sh"
if ! [[ -f "$config" ]] ; then
	echo "$config not found!" 1>&2
	exit 1
fi

source "$config"
aptitude update

# Install some tools
aptitude install -y \
	exim4-daemon-heavy \
	openssl \
	dovecot-imapd dovecot-sieve dovecot-managesieved \
	sa-exim spamassassin spamc \
	swaks htop vim

hostname "$hostname"
echo "$hostname" | cat > /etc/hostname
echo "$mailname" | cat > /etc/mailname

cat > /etc/exim4/update-exim4.conf.conf <<CONF
dc_eximconfig_configtype='internet'
dc_other_hostnames='$other_names'
dc_local_interfaces=''
dc_readhost=''
dc_relay_domains=''
dc_minimaldns='false'
dc_relay_nets=''
dc_smarthost=''
CFILEMODE='644'
dc_use_split_config='true'
dc_hide_mailname=''
dc_mailname_in_oh='true'
dc_localdelivery='maildir_home'
CONF

cat > /etc/exim4/conf.d/router/901_catchall <<CONF
catchall:
  driver = redirect
  domains = +local_domains
  data = $catchall_address
CONF


update-exim4.conf


# Set up TLS

CERT=/etc/exim4/exim.crt
KEY=/etc/exim4/exim.key
if ! [ -f "$CERT" ] || ! [ -f "$KEY" ] ; then
	SSLEAY="$(tempfile -m600 -pexi)"
	cat > $SSLEAY <<EOM
RANDFILE = $HOME/.rnd
[ req ]
default_bits = 1024
default_keyfile = exim.key
distinguished_name = req_distinguished_name
[ req_distinguished_name ]
countryName = Country Code (2 letters)
countryName_default = AU
countryName_min = 2
countryName_max = 2
stateOrProvinceName = State or Province Name (full name)
localityName = Locality Name (eg, city)
organizationName = Organization Name (eg, company; recommended)
organizationName_max = 64
organizationalUnitName = Organizational Unit Name (eg, section)
organizationalUnitName_max = 64
commonName = Server name (eg. ssl.domain.tld; required!!!)
commonName_max = 64
emailAddress = Email Address
emailAddress_max = 40
EOM

	openssl req -config "$SSLEAY" \
		-x509 -newkey rsa:1024 \
		-keyout "$KEY" -out "$CERT" \
		-days "$csr_days" -nodes \
		 -subj "$csr_subject"

	rm -f "$SSLEAY"
	chown root:Debian-exim "$KEY" "$CERT"
	chmod 640 "$KEY" "$CERT"

fi

cat > /etc/exim4/conf.d/main/001_local <<LOCAL
MAIN_TLS_ENABLE = 1
LOCAL


# Set up dovecot

cat > /etc/dovecot/local.conf <<LOCAL
protocol lda {
	mail_plugins = sieve
	postmaster_address = $postmaster_address
}
protocols = "imap sieve"

# Disable non-ssl login
service imap-login {
	inet_listener imap {
		port=0
	}
}

ssl_cert = </etc/dovecot/dovecot.pem
ssl_key = </etc/dovecot/private/dovecot.pem

mail_location = maildir:~/Mailbox
namespace inbox {
	inbox = yes
	location = 
	mailbox Drafts {
		special_use = \\Drafts
	}
	mailbox Junk {
		special_use = \\Junk
	}
	mailbox Sent {
		special_use = \\Sent
	}
	mailbox "Sent Messages" {
		special_use = \\Sent
	}
	mailbox Trash {
		special_use = \\Trash
	}
	prefix =
}

disable_plaintext_auth = yes
# auth_mechanisms = plain login
service auth {
	unix_listener auth-client {
		mode = 0660
		user = Debian-exim
	}
}
userdb {
	driver = passwd
}
passdb {
	driver = pam
}

# managesieve_notify_capability = mailto
# managesieve_sieve_capability = fileinto reject envelope encoded-character vacation subaddress comparator-i;ascii-numeric relational regex imap4flags copy include variables body enotify environment mailbox date ihave
# plugin {
# 	 sieve = ~/.sieve.d/sieverc
# 	 sieve_dir = ~/.sieve.d/include
# }
LOCAL


# Set up SpamAssassin etc
sed -i 's/^ENABLED=0/ENABLED=1/' /etc/default/spamassassin
sed -i 's/^SAEximRunCond: 0$/# \0/' /etc/exim4/sa-exim.conf



# Restart everything
service dovecot restart
service exim4 restart
service spamassassin restart
